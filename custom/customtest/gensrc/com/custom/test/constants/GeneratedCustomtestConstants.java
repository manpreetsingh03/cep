/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Apr 29, 2020, 10:25:55 AM                   ---
 * ----------------------------------------------------------------
 */
package com.custom.test.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast"})
public class GeneratedCustomtestConstants
{
	public static final String EXTENSIONNAME = "customtest";
	public static class TC
	{
		public static final String MYPRODUCT = "MyProduct".intern();
	}
	public static class Attributes
	{
		// no constants defined.
	}
	
	protected GeneratedCustomtestConstants()
	{
		// private constructor
	}
	
	
}
