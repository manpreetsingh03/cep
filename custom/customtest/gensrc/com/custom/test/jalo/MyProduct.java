/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Apr 29, 2020, 10:25:55 AM                   ---
 * ----------------------------------------------------------------
 */
package com.custom.test.jalo;

import de.hybris.platform.directpersistence.annotation.SLDSafe;
import de.hybris.platform.jalo.Item.AttributeMode;
import de.hybris.platform.jalo.SessionContext;
import de.hybris.platform.jalo.product.Product;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Generated class for type MyProduct.
 */
@SLDSafe
@SuppressWarnings({"unused","cast"})
public class MyProduct extends Product
{
	/** Qualifier of the <code>MyProduct.testAttribute</code> attribute **/
	public static final String TESTATTRIBUTE = "testAttribute";
	protected static final Map<String, AttributeMode> DEFAULT_INITIAL_ATTRIBUTES;
	static
	{
		final Map<String, AttributeMode> tmp = new HashMap<String, AttributeMode>(Product.DEFAULT_INITIAL_ATTRIBUTES);
		tmp.put(TESTATTRIBUTE, AttributeMode.INITIAL);
		DEFAULT_INITIAL_ATTRIBUTES = Collections.unmodifiableMap(tmp);
	}
	@Override
	protected Map<String, AttributeMode> getDefaultAttributeModes()
	{
		return DEFAULT_INITIAL_ATTRIBUTES;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>MyProduct.testAttribute</code> attribute.
	 * @return the testAttribute - My Example String Collection
	 */
	public Collection<String> getTestAttribute(final SessionContext ctx)
	{
		Collection<String> coll = (Collection<String>)getProperty( ctx, "testAttribute".intern());
		return coll != null ? coll : Collections.EMPTY_LIST;
	}
	
	/**
	 * <i>Generated method</i> - Getter of the <code>MyProduct.testAttribute</code> attribute.
	 * @return the testAttribute - My Example String Collection
	 */
	public Collection<String> getTestAttribute()
	{
		return getTestAttribute( getSession().getSessionContext() );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>MyProduct.testAttribute</code> attribute. 
	 * @param value the testAttribute - My Example String Collection
	 */
	public void setTestAttribute(final SessionContext ctx, final Collection<String> value)
	{
		setProperty(ctx, "testAttribute".intern(),value == null || !value.isEmpty() ? value : null );
	}
	
	/**
	 * <i>Generated method</i> - Setter of the <code>MyProduct.testAttribute</code> attribute. 
	 * @param value the testAttribute - My Example String Collection
	 */
	public void setTestAttribute(final Collection<String> value)
	{
		setTestAttribute( getSession().getSessionContext(), value );
	}
	
}
