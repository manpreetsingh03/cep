/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.custom.test.constants;

/**
 * Global class for all Customtest constants. You can add global constants for your extension into this class.
 */
public final class CustomtestConstants extends GeneratedCustomtestConstants
{
	public static final String EXTENSIONNAME = "customtest";

	private CustomtestConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension

	public static final String PLATFORM_LOGO_CODE = "customtestPlatformLogo";
}
