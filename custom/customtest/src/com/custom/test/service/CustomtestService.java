/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.custom.test.service;

public interface CustomtestService
{
	String getHybrisLogoUrl(String logoCode);

	void createLogo(String logoCode);
}
