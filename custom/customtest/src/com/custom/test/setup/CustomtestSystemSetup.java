/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.custom.test.setup;

import static com.custom.test.constants.CustomtestConstants.PLATFORM_LOGO_CODE;

import de.hybris.platform.core.initialization.SystemSetup;

import java.io.InputStream;

import com.custom.test.constants.CustomtestConstants;
import com.custom.test.service.CustomtestService;


@SystemSetup(extension = CustomtestConstants.EXTENSIONNAME)
public class CustomtestSystemSetup
{
	private final CustomtestService customtestService;

	public CustomtestSystemSetup(final CustomtestService customtestService)
	{
		this.customtestService = customtestService;
	}

	@SystemSetup(process = SystemSetup.Process.INIT, type = SystemSetup.Type.ESSENTIAL)
	public void createEssentialData()
	{
		customtestService.createLogo(PLATFORM_LOGO_CODE);
	}

	private InputStream getImageStream()
	{
		return CustomtestSystemSetup.class.getResourceAsStream("/customtest/sap-hybris-platform.png");
	}
}
