/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.custom.test.controller;

import static com.custom.test.constants.CustomtestConstants.PLATFORM_LOGO_CODE;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.custom.test.service.CustomtestService;


@Controller
public class CustomtestHelloController
{
	@Autowired
	private CustomtestService customtestService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String printWelcome(final ModelMap model)
	{
		model.addAttribute("logoUrl", customtestService.getHybrisLogoUrl(PLATFORM_LOGO_CODE));
		return "welcome";
	}
}
